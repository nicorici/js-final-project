import axios from 'axios';

export default class Product {
  constructor(id) {
    this.id = id;
  }

  async getProduct() {
    try {
      const link = `http://localhost:3000/products?id=${this.id}`;
      const res = await axios(link);
      this.collection = res.data[0].collection;
      this.img = res.data[0].img;
      this.price = res.data[0].price;
      this.title = res.data[0].title;
      this.size = res.data[0].size;
      this.sizes = res.data[0].sizes;
    } catch (error) {
      console.log(error);
    }
  }
}
