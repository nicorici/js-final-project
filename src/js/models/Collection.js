import axios from 'axios';

export default class Collection {
  constructor(query) {
    this.query = query;
  }

  async getResults() {
    try {
      this.query = this.query.toLowerCase();
      let link = `http://localhost:3000/products?collection=${this.query}`;
      if (this.query === 'home') {
        link = 'http://localhost:3000/products';
      }
      const res = await axios(link);
      this.result = res.data;
    } catch (error) {
      console.log(error);
    }
  }

  async getSearchResults() {
    try {
      this.query = this.query.toLowerCase();
      const link = `http://localhost:3000/products?q=${this.query}`;
      const res = await axios(link);
      this.result = res.data;
    } catch (error) {
      console.log(error);
    }
  }

  async getResultsOrder(order) {
    try {
      let orderValue;
      if (order === 'low') {
        orderValue = 'asc';
      } else if (order === 'high') {
        orderValue = 'desc';
      }
      this.query = this.query.toLowerCase();
      let link;

      if (this.query !== 'home') {
        if (order === 'random') {
          link = `http://localhost:3000/products?collection=${this.query}`;
        } else {
          link = `http://localhost:3000/products?collection=${this.query}&_sort=price&_order=${orderValue}`;
        }
      } else if (order !== 'random') {
        link = `http://localhost:3000/products?_sort=price&_order=${orderValue}`;
      } else {
        link = 'http://localhost:3000/products';
      }

      const res = await axios(link);
      this.result = res.data;
    } catch (error) {
      console.log(error);
    }
  }
}
