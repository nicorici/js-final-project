export default class Cart {
  constructor() {
    this.items = [];
  }

  addItem(id, title, price, img, size) {
    const item = {
      id, title, price, img, size, quantity: 1, total: price,
    };
    this.items.push(item);
    this.persistData();
    return item;
  }

  deleteItem(id) {
    const index = this.items.findIndex(el => el.id === id);
    this.items.splice(index, 1);
    this.persistData();
  }

  getItemById(id) {
    return this.items.find(el => el.id === id);
  }

  isInCart(id) {
    return this.items.findIndex(el => el.id === id) !== -1;
  }

  getNrOfItems() {
    return this.items.length;
  }

  updateQuantity(id, quantity) {
    this.items.filter(el => el.id === id).forEach((el) => {
      el.quantity = quantity;
      el.total = Math.round(el.price * el.quantity * 100) / 100;
    });
    this.persistData();
  }

  persistData() {
    localStorage.setItem('items', JSON.stringify(this.items));
  }

  readStorage() {
    const storage = JSON.parse(localStorage.getItem('items'));
    if (storage) {
      this.items = storage;
    }
  }

  calculateTotal() {
    let sum = 0;
    this.items.forEach((el) => { sum += el.total; });
    return sum;
  }
}
