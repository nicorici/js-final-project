export const elements = {
    navList : document.querySelector('.nav-list'),
    productGrid : document.querySelector('.main-grid'),
    collectionName : document.querySelector('span.collection-name'),
    breadcrumb : document.querySelector('.main-breadcrumb'),
    mainContainer: document.querySelector('.main-container'),
    sortDropDown : document.querySelector('#sortBy'),
    productContainer : document.querySelector('.main-products'),
    pages : document.querySelector('.main-pages'),
    header : document.querySelector('.main-header'),
    cart : document.querySelector('.top-cart'),
    cartContainer : document.querySelector('.main-cart-container'),
    headerTitle : document.querySelector('.header-title'),
    paginationContainer : document.querySelector('.main-pagination'),
    navBtn : document.querySelector('.nav-menu-button'),
    searchInput : document.querySelector('.top-search-input')
}