import { elements } from './base';

const roundNumber = number => Math.round(number * 100) / 100;

export const renderCartItem = item => `
    <div class="main-cart-item" data-itemid=${item.id}>
        <div class="main-cart-item-image">
            <a class="item-image-wrapper" href="#">
                <img class="single-product-image cart-item" src="${item.img}">
            </a>
        </div>

        <div class="main-cart-item-details">
            <p class="main-cart-item-title">${item.title}</p>
            <span class="main-cart-item-size">${item.size}</span>
            <button type="submit" class="remove-from-cart-btn">Remove</button>
        </div>
        <div class="main-cart-prices">
            <div class="main-cart-item-price">
                <span class="item-header-name">Price</span>
                <span class="item-price">$${roundNumber(item.price)}</span>
            </div>

            <div class="main-cart-item-quantity">
                <span class="item-header-name">Quantity</span>
                <input class="item-quantity" type="number" min="1" max="10" step="1" value="${item.quantity}">
            </div>

            <div class="main-cart-item-price total">
                <span class="item-header-name">Total</span>
                <span class="total-price">$${roundNumber(item.total)}</span>
            </div>
        </div>
    </div>
    `;

export const updateCartValue = (nr) => {
  document.querySelector('.cart-value').innerHTML = nr;
};

export const updateTotal = (total) => {
  document.querySelector('.main-total-value').innerHTML = `$${roundNumber(total)}`;
};

export const renderCart = (items) => {
  let itemsMarkup = '';
  items.forEach((element) => {
    itemsMarkup += renderCartItem(element);
  });

  if (document.querySelector('.main-cart-items')) {
    document.querySelector('.main-cart-items').innerHTML = itemsMarkup;
  } else {
    const markup = `   
    <div class="main-cart-container">
        <div class="main-cart-header">
            <span class="cart-header-product">Product</span>
            <span>Price</span>
            <span>Quantity</span>
            <span>Total</span>
        </div>
    
        <div class="main-cart-items"> 
        ${itemsMarkup}
        </div>
    
        <hr class="border-top desktop">
        <hr class="border-top mobile">
        <div class="main-cart-total">
            <p class="main-cart-total-text"><span class="main-total-title">Subtotal </span>
                <span class="main-total-value">$${roundNumber(1,350.99)}</span>
            </p>
            <div class="main-cart-button">
                <button type="submit" class="shop-btn">Continue shopping</button>
            </div>
        </div>
    </div>
    `;
    elements.mainContainer.insertAdjacentHTML('beforeend', markup);
  }
};

export const clearCart = () => {
  elements.productContainer.classList.remove('hidden');
  elements.productGrid.classList.remove('hidden');
  elements.header.classList.remove('hidden');
  elements.breadcrumb.classList.add('hidden');
  if (document.querySelector('.main-cart-container')) {
    document.querySelector('.main-cart-container').parentNode.removeChild(document.querySelector('.main-cart-container'));
  }
};
