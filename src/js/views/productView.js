import {
  elements,
} from './base';

const generateSizeDropDown = (product) => {
  let sizeMarkup = '';
  product.sizes.forEach((el) => {
    sizeMarkup += `<option value=${el}>${el}</option>`;
  });
  return sizeMarkup;
};

export const renderProduct = (product) => {
  const markup = `
                <div class="main-single-product">
                        <div class="main-single-product-image">
                            <a class="single-product-image-wrapper" href="#">
                                <img class="single-product-image" src="${product.img}">
                            </a>
                        </div>
                        <div class="main-single-product-details">
                            <div class="main-single-product-detail">
                                <h2 class="main-single-product-title">${product.title}</h2>
                                <span class="main-single-product-price">$${product.price}</span>
                            </div>
                            <div class="main-single-product-cart">
                                <div class="main-product-size">
                                    <label for="product-size-select">Size</label>
                                    <select class="size-selector" id="product-size-select">
                                        ${generateSizeDropDown(product)}
                                    </select>
                                </div>
                                <div class="main-product-button">
                                    <button type="submit" class="add-to-cart-btn" data-text="Add to cart" data-text-rm="Remove from cart">Add to cart</button>
                                </div>
                            </div>
                        </div>

                        <div id="myModal" class="modal">
                            <span class="close">&times;</span>
                            <img class="modal-content" id="img01">
                        </div>
                 </div>
    `;
  elements.productContainer.insertAdjacentHTML('beforeend', markup);
};

export const clearAllResult = () => {
  elements.productGrid.innerHTML = '';
  elements.productGrid.classList.add('hidden');
  elements.pages.classList.add('hidden');
  elements.header.classList.add('hidden');
  document.querySelectorAll('.main-single-product').forEach(el => el.remove());
};

export const clearProduct = () => {
  elements.productContainer.classList.add('hidden');
  elements.productGrid.classList.add('hidden');
  elements.header.classList.add('hidden');
  elements.breadcrumb.classList.add('hidden');
};
