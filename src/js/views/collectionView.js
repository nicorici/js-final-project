import {
  elements,
} from './base';

const renderProduct = (product) => {
  const markup = `
    <div class="product-item" data-itemid=${product.id}>
        <div class="product-image-container">
            <a class="product-image-wrapper" href="#${product.id}">
                <img class="product-image" src="${product.img}">
            </a>
        </div>
        <div class="product-title">
            <a href="#${product.id}">${product.title}</a>
        </div>
        <div class="product-price">$${product.price}</div>
    </div>
    `;
  elements.productGrid.insertAdjacentHTML('beforeend', markup);
};

const createButtons = (page) => {
  const prevBtn = document.querySelector('.prev');
  const nextBtn = document.querySelector('.next');
  if (prevBtn) {
    prevBtn.parentNode.removeChild(prevBtn);
  }
  if (nextBtn) {
    nextBtn.parentNode.removeChild(nextBtn);
  }
  const markup = `<a href="#" class="prev disabled round" data-goto=${page - 1}>&#8249;</a>
        <a href="#" class="next disabled round" data-goto=${page + 1}>&#8250;</a>`;
  elements.paginationContainer.insertAdjacentHTML('beforeend', markup);
};

const renderButton = (type) => {
  const prevBtn = document.querySelector('.prev');
  const nextBtn = document.querySelector('.next');
  if (type === 'prev' && prevBtn) {
    prevBtn.classList.remove('disabled');
    prevBtn.classList.add('active');
  } else if (type === 'next' && nextBtn) {
    nextBtn.classList.remove('disabled');
    nextBtn.classList.add('active');
  }
};

const renderButtons = (page, numResults, resPerPage) => {
  createButtons(page);
  const pages = Math.ceil(numResults / resPerPage);
  if (page === 1 && pages > 1) {
    renderButton('next');
  } else if (page < pages) {
    renderButton('prev');
    renderButton('next');
  } else if (page === pages) {
    renderButton('prev');
  }
};

export const renderResult = (products, page = 1, resPerPage = 9) => {
  const start = (page - 1) * resPerPage;
  const end = page * resPerPage;
  products.slice(start, end).forEach(renderProduct);
  if (products.length < 10) {
    elements.paginationContainer.classList.add('hidden');
  } else {
    elements.paginationContainer.classList.remove('hidden');
    renderButtons(page, products.length, resPerPage);
  }
};

export const clearResult = () => {
  elements.productGrid.innerHTML = '';
  elements.productGrid.classList.remove('hidden');
  elements.pages.classList.remove('hidden');
  elements.header.classList.remove('hidden');
  document.querySelectorAll('.main-single-product').forEach(el => el.remove());
};

export const updatePageAfterResult = (collection) => {
  if (collection === 'Home') {
    elements.collectionName.innerHTML = '';
    elements.breadcrumb.innerHTML = 'Home';
  } else {
    elements.collectionName.innerHTML = collection;
    elements.breadcrumb.innerHTML = `Home > ${collection}`;
    /* history.pushState('data to be passed', 'Title of the page', `/collections/${collection}`); */
  }
  elements.sortDropDown.selectedIndex = '0';
};

export const highlightSelected = (collection) => {
  const resultsArr = Array.from(document.querySelectorAll('.nav-link'));
  resultsArr.forEach((el) => {
    el.classList.remove('nav-link-selected');
  });
  resultsArr.forEach((el) => {
    if (el.text === collection) {
      el.classList.add('nav-link-selected');
    }
  });
};
