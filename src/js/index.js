// Global app controller
import Collection from './models/Collection';
import Product from './models/Product';
import Cart from './models/Cart';
import * as collectionView from './views/collectionView';
import * as productView from './views/productView';
import * as cartView from './views/cartView';
import {
  elements,
} from './views/base';


let state = {};

const controlCollection = async (query) => {
  if (query) {
    state.collection = new Collection(query);
    try {
      cartView.clearCart();
      collectionView.clearResult();
      collectionView.highlightSelected(query);
      await state.collection.getResults();
      collectionView.renderResult(state.collection.result);
      collectionView.updatePageAfterResult(query);
    } catch (err) {
      console.log(err);
      console.error('Error getting a collection');
    }
  }
};

const controlSearchCollection = async (query) => {
  if (query) {
    state.collection = new Collection(query);
    try {
      cartView.clearCart();
      collectionView.clearResult();
      await state.collection.getSearchResults();
      collectionView.renderResult(state.collection.result);
      collectionView.updatePageAfterResult(query);
    } catch (err) {
      console.log(err);
      console.error('Error getting a collection');
    }
  }
};

const controlCollectionOrdered = async (query, order) => {
  if (query) {
    state.collection = new Collection(query);
    try {
      cartView.clearCart();
      collectionView.clearResult();
      await state.collection.getResultsOrder(order);
      collectionView.renderResult(state.collection.result);
    } catch (err) {
      console.log(err);
      console.error('Error getting a collection');
    }
  }
};

const controlProduct = async (id) => {
  if (id) {
    state.product = new Product(id);
    try {
      cartView.clearCart();
      productView.clearAllResult();
      await state.product.getProduct();
      productView.renderProduct(state.product);
    } catch (err) {
      console.log(err);
      console.error('Error getting a product');
    }
  }
};

const controlCartItem = () => {
  if (!state.cart) {
    state.cart = new Cart();
  }
  const prod = state.product;
  if (!state.cart.isInCart(prod.id)) {
    state.cart.addItem(prod.id, prod.title, prod.price, prod.img, prod.size);
  }
  cartView.updateCartValue(state.cart.getNrOfItems());
};

const controlCart = () => {
  if (!state.cart) {
    state.cart = new Cart();
  }
  productView.clearProduct();
  cartView.renderCart(state.cart.items);
  cartView.updateTotal(state.cart.calculateTotal());
  cartView.updateCartValue(state.cart.getNrOfItems());
};

elements.navList.addEventListener('click', (e) => {
  if (e.target.matches('.nav-item, .nav-item *')) {
    let selected;
    if (e.target.text) {
      selected = e.target.text;
    } else {
      selected = e.target.querySelector('.nav-link').text;
    }
    controlCollection(selected);
  }
});

elements.sortDropDown.addEventListener('change', (e) => {
  if (e.target.matches('.collection-sort-dropdown, .collection-sort-dropdown *')) {
    const order = e.target.options[e.target.selectedIndex].value;
    const query = elements.collectionName.innerText === '' ? 'home' : elements.collectionName.innerText;
    controlCollectionOrdered(query, order);
  }
});

window.addEventListener('load', () => {
  state.cart = new Cart();
  state.cart.readStorage();
  cartView.updateCartValue(state.cart.getNrOfItems());
  controlCollection('Home');
});

elements.mainContainer.addEventListener('click', (e) => {
  const productDiv = e.target.closest('.product-image-container, .product-title');
  if (productDiv) {
    const id = productDiv.parentElement.dataset.itemid;
    controlProduct(id);
  }

  const addBtn = e.target.closest('.main-product-button, .main-product-button *');
  if (addBtn) {
    if(addBtn.tagName !== 'BUTTON'){
      addBtn = addBtn.children[0];
    }
    if(addBtn.getAttribute('data-text') === addBtn.innerHTML){
      controlCartItem();
      addBtn.innerHTML = addBtn.getAttribute('data-text-rm');
    }else{
      addBtn.setAttribute('data-text-rm', addBtn.innerHTML);
      state.cart.deleteItem(state.product.id);
      addBtn.innerHTML = addBtn.getAttribute('data-text');
      cartView.updateCartValue(state.cart.getNrOfItems());
    }
  }

  const removeCartBtn = e.target.closest('.remove-from-cart-btn');
  if (removeCartBtn) {
    const id = removeCartBtn.parentElement.parentElement.dataset.itemid;
    console.log(id);
    state.cart.deleteItem(id);
    controlCart();
  }

  const pagContainer = e.target.closest('.main-pagination');
  if (pagContainer) {
    let btn;
    if (e.target.closest('.prev')) {
      btn = e.target.closest('.prev');
    } else if (e.target.closest('.next')) {
      btn = e.target.closest('.next');
    }
    if (btn) {
      const goTo = parseInt(btn.dataset.goto, 10);
      if (goTo > 0 && goTo <= Math.ceil(state.collection.result.length / 9)) {
        collectionView.clearResult();
        collectionView.renderResult(state.collection.result, goTo);
      }
    }
  }

  const img = e.target.closest('.single-product-image');
  if (img) {
    const modalImg = document.getElementById('img01');
    const captionText = document.getElementById('caption');
    const modal = document.getElementById('myModal');
    modal.classList.add('not-hidden');
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
  }

  const closeBtn = e.target.closest('.close');
  if (closeBtn) {
    document.getElementById('myModal').classList.remove('not-hidden');
    document.getElementById('myModal').classList.add('hidden');
  }

  const continueBtn = e.target.closest('.shop-btn');
  if(continueBtn){
    controlCollection('Home');
  }
});

elements.mainContainer.addEventListener('change', (e) => {
  const sizeSelector = e.target.closest('.size-selector');
  if (sizeSelector) {
    state.product.size = e.target.value;
  }
});

elements.mainContainer.addEventListener('input', (e) => {
  const quantityInput = e.target.closest('.item-quantity');
  if (quantityInput) {
    const id = quantityInput.parentElement.parentElement.parentElement.dataset.itemid;
    state.cart.updateQuantity(id, e.target.value);
    cartView.updateTotal(state.cart.calculateTotal());
    quantityInput.parentNode.parentNode.children[2].children[1].innerHTML = `$${state.cart.getItemById(id).total}`;
  }
});

elements.cart.addEventListener('click', () => {
  controlCart();
});

elements.headerTitle.addEventListener('click', () => {
  controlCollection('Home');
});

elements.navBtn.addEventListener('click', () => {
  elements.navList.classList.toggle('nav-list-mobile');
});

elements.searchInput.addEventListener('change', () => {
  const query = elements.searchInput.value;
  controlSearchCollection(query);
});
